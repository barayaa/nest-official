import { PartialType } from "@nestjs/mapped-types";
import { CreateCoffeeDto } from "./create-dto";

export class UpdateCoffeeDto extends PartialType(CreateCoffeeDto){}